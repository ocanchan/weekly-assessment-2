# Task 1: Understanding Javascript
What is the difference between Javascript and HTML?
Javascript adalah bahasa script yang menambahkan dinamik dari sebuah website sedangkan, sedangkan HTML adalah bahasa markup yang menyediakan struktur dasar dari sebuah website

What is the differenece between var, let, and const?
ada 2 jenis perbedaan antara var, let dan const.
a.perbedaan pertama antara var dgn let,const adalah scope dari variable tersebut
var memiliki global scope yang bisa diakses diluar code block tersebut

sementara let,const memiliki scope local atau hanya dalam code block tersebut

b.perbedaan kedua antara var,let dgn const adalah kondisi dari variable tersebut
var, let dapat diubah value dari variable tersebut

sementara const sesuai namanya constant, tidak bisa diganti kecuali melalu mutasi.

What data types in javascript do you know?
untuk primitive : string("contoh",'ini',`string`), number(1,2.5,-1), boolean(true,false)

untuk non-primitive : objek, dan array

What do you know about function?
function adalah sebuah fungsi yang biasanya berisi code atau syntax yang bisa dipanggil kembali, yang biasanya berisi parameter sebagai variable dalam function tersebut.

What do you know about hoisting?
Hoisting itu adalah behaviour supaya variable menjadi global scope dengan cara mendeklarasikan variable tersebut sebelum code di inisialisasi