// write function here
const oddEven = (angka) => {
  if(angka % 2 === 0){
    console.log(`${angka} is even number`);
  } else {
    console.log(`${angka} is odd number`)
  }
  return;
};

// input test
const input1 = 3;
const input2 = 10;

oddEven(input1); // output: 3 is odd number
